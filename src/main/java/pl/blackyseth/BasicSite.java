package pl.blackyseth;

import org.hibernate.Session;
import pl.blackyseth.domain.Config;
import pl.blackyseth.domain.User;

import javax.annotation.Resource;
import javax.ejb.*;
import javax.persistence.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.*;
import javax.transaction.RollbackException;
import java.io.IOException;

@Stateless
@WebServlet(name = "BasicSite", urlPatterns = "/BasicSite")
public class BasicSite extends HttpServlet {

    @PersistenceContext
    protected EntityManager entityManager;
    @Resource
    private UserTransaction tx;

    @Override
    public void init() throws ServletException {

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Config.getInstance().contextInitialized();
        User user = new User();
        user.setId(0);
        user.setUsername("lol");


        try {
            tx.begin();
            entityManager.persist(user);
            tx.commit();

        } catch (Exception e){
            e.printStackTrace();
        }



        resp.sendRedirect("index.jsp");

    }


}
