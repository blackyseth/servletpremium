package pl.blackyseth.domain;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class Config{
    private EntityManager entityManager;
    private EntityManagerFactory entityManagerFactory;
    private static Config config = new Config();

    private void Config(){

    }

    public void contextInitialized() {
        this.entityManagerFactory = Persistence.createEntityManagerFactory("Servletovo");
        this.entityManager = entityManagerFactory.createEntityManager();
    }


    public void contextDestroyed() {
        entityManager.close();
        entityManagerFactory.close();
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public static Config getInstance() {
        if(config!=null){
            return config;
        }else {
            return new Config();
        }

    }
}
