package pl.blackyseth.domain;


import java.util.ArrayList;
import java.util.List;

public class DummyDB {
    private List<User> users = new ArrayList<User>();
    private static DummyDB dummyDB;

    private DummyDB() {
    }

    public User getUser(int id){
        return users.get(id);
    }

    public static DummyDB getInstance(){
        if(dummyDB==null){
            dummyDB = new DummyDB();
        }
        return dummyDB;
    }

    public void addUser(User user){
        users.add(user);
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
