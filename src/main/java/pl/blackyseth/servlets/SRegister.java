package pl.blackyseth.servlets;

import pl.blackyseth.domain.DummyDB;
import pl.blackyseth.domain.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(urlPatterns = "/Register", name = "Register")
public class SRegister extends HttpServlet {

    int i;
    @Override
    public void init() throws ServletException {
        this.i=0;
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        DummyDB dummyDB = DummyDB.getInstance();
        User user = new User();
        HttpSession session = req.getSession();
        user.setId(i);
        user.setUsername((String)req.getParameter("username"));
        user.setPassword((String)req.getParameter("password"));
        user.setEmail((String)req.getParameter("email"));
        dummyDB.addUser(user);
        resp.sendRedirect("users.jsp");
        i++;
    }
}