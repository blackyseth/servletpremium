package pl.blackyseth.servlets;

import pl.blackyseth.domain.DummyDB;
import pl.blackyseth.domain.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/Users", name = "Users")
public class SUsers extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        DummyDB dummyDB = DummyDB.getInstance();
        User user = dummyDB.getUser(Integer.valueOf(req.getParameter("option")));
        if(Boolean.valueOf(req.getParameter("premium"))){
            user.setPremium(true);
        }else{
            user.setPremium(false);
        }

    }
}
