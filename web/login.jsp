<%--
  Created by IntelliJ IDEA.
  User: jjayd
  Date: 13.12.2015
  Time: 20:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="css/style.css">
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <title>Log in</title>
</head>
<body>
<div class="logo"></div>
<div class="login-block">
    <form action="Login" method="post">
    <h1>Login</h1>
    <input type="text" value="" placeholder="Username" name="login" id="username" />
    <input type="password" value="" placeholder="Password" name="pass" id="password" />
    <button>Submit</button>
    </form>
</div>
<br/>
<br/>
<div class="login-block">
    <a href="index.jsp"><h1>index</h1></a>
    <a href="login.jsp"><h1>login</h1></a>
    <a href="register.jsp"><h1>register</h1></a>
    <a href="permissions.jsp"><h1>permissions</h1></a>
    <a href="users.jsp"><h1>users</h1></a>
</div>
</body>
</html>

