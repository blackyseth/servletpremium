<%@ page import="pl.blackyseth.domain.DummyDB" %>
<%@ page import="pl.blackyseth.domain.User" %><%--
  Created by IntelliJ IDEA.
  User: jjayd
  Date: 13.12.2015
  Time: 20:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="css/style.css">
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <title>Permissions</title>
    <style>
        .login-block input#isPremium {
            width: 40%;
            display: inline;
        }

        .login-block input#isNotPremium {
            width: 40%;
            display: inline;
        }
    </style>
</head>
<body>
<div class="logo"></div>
<div class="login-block">
    <form method="post" action="Users">
        <h1>Permissions</h1>
        <select name="option">
            <%
                for (User u:
                        DummyDB.getInstance().getUsers()) {


            %>
            <option value="<%= u.getId()%>"><%= u.getUsername()%></option>
            <%
                }
            %>
        </select>
        <input type="radio" value="true" name="premium" placeholder="Premium" id="isPremium" />Premium
        <input type="radio" value="false" name="premium" placeholder="Not Premium" id="isNotPremium" />Not Premium
        <button>Submit</button>
    </form>
</div>
<br/>
<br/>
<div class="login-block">
    <a href="index.jsp"><h1>index</h1></a>

    <a href="login.jsp"><h1>login</h1></a>

    <a href="register.jsp"><h1>register</h1></a>

    <a href="permissions.jsp"><h1>permissions</h1></a>
    <a href="users.jsp"><h1>users</h1></a>
</div>
</body>
</html>

